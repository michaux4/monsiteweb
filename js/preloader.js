// OffLine

/* setTimeout(() => {
    window.scrollTo(0, 0);
    document.querySelector(".preloader").classList.add('disapear');

    setTimeout(() => {
        document.querySelector(".preloader").style.display = "none";
    }, 2000);
}, 2000); */

// OnLine
window.onload = (() => {

    window.scrollTo(0, 0);
    document.querySelector(".preloader").classList.add('disapear');

    setTimeout(() => {
        document.querySelector(".preloader").style.display = "none";
    }, 2000);


    // TextAutoWhrite
    var elements = document.getElementsByClassName('txt-rotate');
    for (var i = 0; i < elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-rotate');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
            new TxtRotate(elements[i], JSON.parse(toRotate), period);
        }
    }
    // INJECT CSS
    var css = document.createElement("style");
    css.type = "text/css";
    css.innerHTML = ".txt-rotate > .wrap { border-right: 0.08em solid #DCDCDC }";
    document.body.appendChild(css);

})