//Fonction de lecture de fichier Json 

// Fonction de lecture du fichier Json
function ReadJson(lang) {
    var config = "";
    fetch("./data/lang.json").then((response) => {
        response.json().then((data) => {

            if (lang === "de") config = data.de;
            if (lang === "fr") config = data.fr;
            if (lang === "en") config = data.en;

            //Configuration des balises

            let page = document.getElementById("idPage").innerHTML;
            switch (page) {
                case "home": pageHome(config); break;
                case "experience": pageExperience(config); break;
                case "education": pageEducation(config); break;
                case "soft": pageSoft(config); break;
                case "programmation": pageProgrammation(config); break;
                case "portfolio": pagePortfolio(config); break;
            }
        })
    })
}

function pagePortfolio(config) {

    document.querySelector('title').innerHTML = config[0].portfolio[0].title;
    document.getElementById("myHtml").setAttribute("lang", config[0].portfolio[0].lang);
    document.getElementById("myDescription").setAttribute("content", config[0].portfolio[0].description);
    document.getElementById("myKeywords").setAttribute("content", config[0].portfolio[0].keywords);

    document.getElementById("dataBox1Image").setAttribute("src", config[0].portfolio[0].box1Image);
    document.getElementById("dataBox2Image").setAttribute("src", config[0].portfolio[0].box2Image);
    document.getElementById("dataBox3Image").setAttribute("src", config[0].portfolio[0].box3Image);
    document.getElementById("dataBox4Image").setAttribute("src", config[0].portfolio[0].box4Image);
    document.getElementById("dataBox5Image").setAttribute("src", config[0].portfolio[0].box5Image);
    document.getElementById("dataBox6Image").setAttribute("src", config[0].portfolio[0].box6Image);
    document.getElementById("dataBox7Image").setAttribute("src", config[0].portfolio[0].box7Image);

    document.querySelector('#dataBox1Title').innerHTML = config[0].portfolio[0].box1Title;
    document.querySelector('#dataBox2Title').innerHTML = config[0].portfolio[0].box2Title;
    document.querySelector('#dataBox3Title').innerHTML = config[0].portfolio[0].box3Title;
    document.querySelector('#dataBox4Title').innerHTML = config[0].portfolio[0].box4Title;
    document.querySelector('#dataBox5Title').innerHTML = config[0].portfolio[0].box5Title;
    document.querySelector('#dataBox6Title').innerHTML = config[0].portfolio[0].box6Title;
    document.querySelector('#dataBox7Title').innerHTML = config[0].portfolio[0].box7Title;

    document.querySelector('#dataBox1Text').innerHTML = config[0].portfolio[0].box1Text;
    document.querySelector('#dataBox2Text').innerHTML = config[0].portfolio[0].box2Text;
    document.querySelector('#dataBox3Text').innerHTML = config[0].portfolio[0].box3Text;
    document.querySelector('#dataBox4Text').innerHTML = config[0].portfolio[0].box4Text;
    document.querySelector('#dataBox5Text').innerHTML = config[0].portfolio[0].box5Text;
    document.querySelector('#dataBox6Text').innerHTML = config[0].portfolio[0].box6Text;
    document.querySelector('#dataBox7Text').innerHTML = config[0].portfolio[0].box7Text;

    document.querySelector('#dataButton1').innerHTML = config[0].portfolio[0].button1;
    document.querySelector('#dataButton2').innerHTML = config[0].portfolio[0].button2;
    document.querySelector('#dataButton3').innerHTML = config[0].portfolio[0].button3;
    document.querySelector('#dataButton4').innerHTML = config[0].portfolio[0].button4;
    document.querySelector('#dataButton5').innerHTML = config[0].portfolio[0].button5;
    document.querySelector('#dataButton6').innerHTML = config[0].portfolio[0].button6;
    document.querySelector('#dataButton7').innerHTML = config[0].portfolio[0].button7;


}

function pageProgrammation(config) {

    document.querySelector('title').innerHTML = config[0].programmation[0].title;
    document.getElementById("myHtml").setAttribute("lang", config[0].programmation[0].lang);
    document.getElementById("myDescription").setAttribute("content", config[0].programmation[0].description);
    document.getElementById("myKeywords").setAttribute("content", config[0].programmation[0].keywords);
    document.querySelector('#dataTitlePage').innerHTML = config[0].programmation[0].titlePage;

    document.getElementById("dataBox1Image").setAttribute("src", config[0].programmation[0].box1Image);
    document.getElementById("dataBox2Image").setAttribute("src", config[0].programmation[0].box2Image);
    document.getElementById("dataBox3Image").setAttribute("src", config[0].programmation[0].box3Image);
    document.getElementById("dataBox4Image").setAttribute("src", config[0].programmation[0].box4Image);
    document.getElementById("dataBox5Image").setAttribute("src", config[0].programmation[0].box5Image);
    document.getElementById("dataBox6Image").setAttribute("src", config[0].programmation[0].box6Image);
    document.getElementById("dataBox7Image").setAttribute("src", config[0].programmation[0].box7Image);
    document.getElementById("dataBox8Image").setAttribute("src", config[0].programmation[0].box8Image);
    document.getElementById("dataBox9Image").setAttribute("src", config[0].programmation[0].box9Image);

    document.querySelector('#dataBox1Title').innerHTML = config[0].programmation[0].box1Title;
    document.querySelector('#dataBox2Title').innerHTML = config[0].programmation[0].box2Title;
    document.querySelector('#dataBox3Title').innerHTML = config[0].programmation[0].box3Title;
    document.querySelector('#dataBox4Title').innerHTML = config[0].programmation[0].box4Title;
    document.querySelector('#dataBox5Title').innerHTML = config[0].programmation[0].box5Title;
    document.querySelector('#dataBox6Title').innerHTML = config[0].programmation[0].box6Title;
    document.querySelector('#dataBox7Title').innerHTML = config[0].programmation[0].box7Title;
    document.querySelector('#dataBox8Title').innerHTML = config[0].programmation[0].box8Title;
    document.querySelector('#dataBox9Title').innerHTML = config[0].programmation[0].box9Title;

    document.querySelector('#dataBox1Text').innerHTML = config[0].programmation[0].box1Text;
    document.querySelector('#dataBox2Text').innerHTML = config[0].programmation[0].box2Text;
    document.querySelector('#dataBox3Text').innerHTML = config[0].programmation[0].box3Text;
    document.querySelector('#dataBox4Text').innerHTML = config[0].programmation[0].box4Text;
    document.querySelector('#dataBox5Text').innerHTML = config[0].programmation[0].box5Text;
    document.querySelector('#dataBox6Text').innerHTML = config[0].programmation[0].box6Text;
    document.querySelector('#dataBox7Text').innerHTML = config[0].programmation[0].box7Text;
    document.querySelector('#dataBox8Text').innerHTML = config[0].programmation[0].box8Text;
    document.querySelector('#dataBox9Text').innerHTML = config[0].programmation[0].box9Text;


}

function pageSoft(config) {

    document.querySelector('title').innerHTML = config[0].soft[0].title;
    document.getElementById("myHtml").setAttribute("lang", config[0].soft[0].lang);
    document.getElementById("myDescription").setAttribute("content", config[0].soft[0].description);
    document.getElementById("myKeywords").setAttribute("content", config[0].soft[0].keywords);
    document.querySelector('#dataTitlePage').innerHTML = config[0].soft[0].titlePage;

    document.querySelector('#dataTitle1').innerHTML = config[0].soft[0].title1;
    document.querySelector('#dataTitle2').innerHTML = config[0].soft[0].title2;
    document.querySelector('#dataTitle3').innerHTML = config[0].soft[0].title3;
    document.querySelector('#dataTitle4').innerHTML = config[0].soft[0].title4;

    document.querySelector('#dataT1Text1').inner = config[0].soft[0].t1Text1;
    document.querySelector('#dataT1Text2').innerHTML = config[0].soft[0].t1Text2;
    document.querySelector('#dataT1Text3').innerHTML = config[0].soft[0].t1Text3;
    document.querySelector('#dataT1Text4').innerHTML = config[0].soft[0].t1Text4;
    document.querySelector('#dataT1Text5').innerHTML = config[0].soft[0].t1Text5;
    document.querySelector('#dataT1Text6').innerHTML = config[0].soft[0].t1Text6;
    document.querySelector('#dataT1Text7').innerHTML = config[0].soft[0].t1Text7;
    document.querySelector('#dataT1Text8').innerHTML = config[0].soft[0].t1Text8;
    document.querySelector('#dataT1Text9').innerHTML = config[0].soft[0].t1Text9;

    document.querySelector('#dataT2Text').innerHTML = config[0].soft[0].t2Text;

    document.querySelector('#dataT3Text1').innerHTML = config[0].soft[0].t3Text1;
    document.querySelector('#dataT3Text2').innerHTML = config[0].soft[0].t3Text2;
    document.querySelector('#dataT3Text3').innerHTML = config[0].soft[0].t3Text3;
    document.querySelector('#dataT3Text4').innerHTML = config[0].soft[0].t3Text4;
    document.querySelector('#dataT3Text5').innerHTML = config[0].soft[0].t3Text5;

    document.querySelector('#dataT4Text').innerHTML = config[0].soft[0].t4Text;


}

function pageEducation(config) {

    document.querySelector('title').innerHTML = config[0].diplome[0].title;
    document.getElementById("myHtml").setAttribute("lang", config[0].diplome[0].lang);
    document.getElementById("myDescription").setAttribute("content", config[0].diplome[0].description);
    document.getElementById("myKeywords").setAttribute("content", config[0].diplome[0].keywords);
    document.querySelector('#dataTitlePage').innerHTML = config[0].diplome[0].titlePage;
    document.querySelector('#dataIntro').innerHTML = config[0].diplome[0].intro;
    document.querySelector('#dataTitle1').innerHTML = config[0].diplome[0].title1;
    document.querySelector('#dataT1Text').innerHTML = config[0].diplome[0].t1Text;
    document.querySelector('#dataTitle2').innerHTML = config[0].diplome[0].title2;
    document.querySelector('#dataT2Text').innerHTML = config[0].diplome[0].t2Text;
    document.querySelector('#dataTitle3').innerHTML = config[0].diplome[0].title3;
    document.querySelector('#dataT3Text').innerHTML = config[0].diplome[0].t3Text;


}

function pageExperience(config) {

    document.querySelector('title').innerHTML = config[0].experience[0].title;
    document.getElementById("myHtml").setAttribute("lang", config[0].experience[0].lang);
    document.getElementById("myDescription").setAttribute("content", config[0].experience[0].description);
    document.getElementById("myKeywords").setAttribute("content", config[0].experience[0].keywords);
    document.querySelector('#dataTitlePage').innerHTML = config[0].experience[0].titlePage;
    document.querySelector('#dataTitle1').innerHTML = config[0].experience[0].title1;

    document.querySelector('#dataBox1').innerHTML = config[0].experience[0].box1;
    document.querySelector('#dataCompagny1').innerHTML = config[0].experience[0].compagny1;
    document.querySelector('#dataJob1').innerHTML = config[0].experience[0].job1;
    document.querySelector('#dataBoxText1').innerHTML = config[0].experience[0].boxText1;

    document.querySelector('#dataBox2').innerHTML = config[0].experience[0].box2;
    document.querySelector('#dataCompagny2').innerHTML = config[0].experience[0].compagny2;
    document.querySelector('#dataJob2').innerHTML = config[0].experience[0].job2;
    document.querySelector('#dataBoxText2').innerHTML = config[0].experience[0].boxText2;

    document.querySelector('#dataBox3').innerHTML = config[0].experience[0].box3;
    document.querySelector('#dataCompagny3').innerHTML = config[0].experience[0].compagny3;
    document.querySelector('#dataJob3').innerHTML = config[0].experience[0].job3;
    document.querySelector('#dataBoxText3').innerHTML = config[0].experience[0].boxText3;

    document.querySelector('#dataBox4').innerHTML = config[0].experience[0].box4;
    document.querySelector('#dataCompagny4').innerHTML = config[0].experience[0].compagny4;
    document.querySelector('#dataJob4').innerHTML = config[0].experience[0].job4;
    document.querySelector('#dataBoxText4').innerHTML = config[0].experience[0].boxText4;

    document.querySelector('#dataBox5').innerHTML = config[0].experience[0].box5;
    document.querySelector('#dataCompagny5').innerHTML = config[0].experience[0].compagny5;
    document.querySelector('#dataJob5').innerHTML = config[0].experience[0].job5;
    document.querySelector('#dataBoxText5').innerHTML = config[0].experience[0].boxText5;
    document.querySelector('#dataBoxText51').innerHTML = config[0].experience[0].boxText51;

    document.querySelector('#dataTitle2').innerText = config[0].experience[0].title2;

    document.querySelector('#dataBox6').innerHTML = config[0].experience[0].box6;
    document.querySelector('#dataCompagny6').innerHTML = config[0].experience[0].compagny6;
    document.querySelector('#dataJob6').innerHTML = config[0].experience[0].job6;
    document.querySelector('#dataBoxText6').innerHTML = config[0].experience[0].boxText6;

    document.querySelector('#dataBox7').innerHTML = config[0].experience[0].box7;
    document.querySelector('#dataCompagny7').innerHTML = config[0].experience[0].compagny7;
    document.querySelector('#dataJob7').innerHTML = config[0].experience[0].job7;
    document.querySelector('#dataBoxText7').innerHTML = config[0].experience[0].boxText7;

    document.querySelector('#dataBox8').innerHTML = config[0].experience[0].box8;
    document.querySelector('#dataCompagny8').innerHTML = config[0].experience[0].compagny8;
    document.querySelector('#dataJob8').innerHTML = config[0].experience[0].job8;
    document.querySelector('#dataBoxText8').innerHTML = config[0].experience[0].boxText8;

    document.querySelector('#dataBox9').innerHTML = config[0].experience[0].box9;
    document.querySelector('#dataCompagny9').innerHTML = config[0].experience[0].compagny9;
    document.querySelector('#dataJob9').innerHTML = config[0].experience[0].job9;
    document.querySelector('#dataBoxText9').innerHTML = config[0].experience[0].boxText9;

    document.querySelector('#dataBox10').innerHTML = config[0].experience[0].box10;
    document.querySelector('#dataCompagny10').innerHTML = config[0].experience[0].compagny10;
    document.querySelector('#dataJob10').innerHTML = config[0].experience[0].job10;
    document.querySelector('#dataBoxText10').innerHTML = config[0].experience[0].boxText10;

    document.querySelector('#dataBox11').innerHTML = config[0].experience[0].box11;
    document.querySelector('#dataCompagny11').innerHTML = config[0].experience[0].compagny11;
    document.querySelector('#dataJob11').innerHTML = config[0].experience[0].job11;
    document.querySelector('#dataBoxText11').innerHTML = config[0].experience[0].boxText11;

    document.querySelector('#dataTitle3').innerHTML = config[0].experience[0].title3;

    document.querySelector('#dataBox12').innerHTML = config[0].experience[0].box12;
    document.querySelector('#dataCompagny12').innerHTML = config[0].experience[0].compagny12;
    document.querySelector('#dataJob12').innerHTML = config[0].experience[0].job12;
    document.querySelector('#dataBoxText12').innerHTML = config[0].experience[0].boxText12;

    document.querySelector('#dataBox13').innerHTML = config[0].experience[0].box13;
    document.querySelector('#dataCompagny13').innerHTML = config[0].experience[0].compagny13;
    document.querySelector('#dataJob13').innerHTML = config[0].experience[0].job13;
    document.querySelector('#dataBoxText13').innerHTML = config[0].experience[0].boxText13;


}

function pageHome(config) {

    document.querySelector('title').textContent = config[0].home[0].title;
    document.getElementById("myHtml").setAttribute("lang", config[0].home[0].lang);
    document.getElementById("myDescription").setAttribute("content", config[0].home[0].description);
    document.getElementById("myKeywords").setAttribute("content", config[0].home[0].keywords);

    document.querySelector('#dataHome').innerHTML = config[0].home[0].home;
    document.querySelector('#dataAbout').innerHTML = config[0].home[0].about;
    document.querySelector('#dataResume').innerHTML = config[0].home[0].resume;
    document.querySelector('#dataContact').innerHTML = config[0].home[0].contact;

    document.getElementById("dataTitleButton").innerHTML = config[0].home[0].titleButton;
    document.querySelector('#dataAboutTitle').innerHTML = config[0].home[0].aboutTitle;
    document.querySelector('#dataTextAbout1').innerText = config[0].home[0].textAbout1;
    document.querySelector('#dataTextAbout2').innerText = config[0].home[0].textAbout2;
    document.querySelector('#dataAboutLi1').innerText = config[0].home[0].aboutLi1;
    document.querySelector('#dataAboutLi2').innerText = config[0].home[0].aboutLi2;
    document.querySelector('#dataAboutLi3').innerText = config[0].home[0].aboutLi3;
    document.querySelector('#dataAboutLi4').innerHTML = config[0].home[0].aboutLi4;

    document.querySelector('#dataResumeTitle').innerHTML = config[0].home[0].resumeTitle;
    document.querySelector('#databox1').innerHTML = config[0].home[0].box1;
    document.querySelector('#dataBoxText1').innerHTML = config[0].home[0].boxText1;
    document.querySelector('#databox2').innerHTML = config[0].home[0].box2;
    document.querySelector('#dataBoxText2').innerHTML = config[0].home[0].boxText2;
    document.querySelector('#databox3').innerHTML = config[0].home[0].box3;
    document.querySelector('#dataBoxText3').innerHTML = config[0].home[0].boxText3;
    document.querySelector('#databox4').innerHTML = config[0].home[0].box4;
    document.querySelector('#dataBoxText4').innerHTML = config[0].home[0].boxText4;
    document.querySelector('#databox5').innerHTML = config[0].home[0].box5;
    document.querySelector('#dataBoxText5').innerHTML = config[0].home[0].boxText5;
    document.querySelector('#dataButton1').innerHTML = config[0].home[0].button;
    document.querySelector('#dataButton2').innerHTML = config[0].home[0].button;
    document.querySelector('#dataButton3').innerHTML = config[0].home[0].button;
    document.querySelector('#dataButton4').innerHTML = config[0].home[0].button;
    document.querySelector('#dataButton5').innerHTML = config[0].home[0].button;

    document.querySelector('#dataAdresseTitle').innerHTML = config[0].home[0].adresseTitle;
    document.querySelector('#dataContactTitle').innerHTML = config[0].home[0].contactTitle;
    document.querySelector('#dataAdresse').innerHTML = config[0].home[0].adresse;
    document.querySelector('#dataEmailTel').innerHTML = config[0].home[0].emailTel;

    document.getElementById("dataMap").setAttribute("src", config[0].home[0].map);

}

if (document.querySelector('#idPage').innerHTML === "home") {

    //Fonction de changement de langue en fonction d'un click
    document.getElementById("langFR").addEventListener("click", function () { changeLang("fr"); }, false);
    document.getElementById("langDE").addEventListener("click", function () { changeLang("de"); }, false);
    document.getElementById("langEN").addEventListener("click", function () { changeLang("en"); }, false);


    function changeLang(lang) {
        sessionStorage.setItem("GlobalLang", lang);
        ReadJson(lang);
    }


    // Voir la langue du client est

    console.log("variable globale:" + sessionStorage.getItem("GlobalLang"))

     if (sessionStorage.getItem("GlobalLang") !== "de") {
         console.log("reussi")
         lang = sessionStorage.getItem("GlobalLang");
     }

    if (typeof lang == "undefined" || lang == null) {

        const language = navigator.language;

        if (language.indexOf('de') > -1) {
            lang = "de";
            ReadJson(lang);
        };

        if (language.indexOf('fr') > -1) {
            lang = "fr";
            ReadJson(lang);
        }

        if (language.indexOf('en') > -1) {
            lang = "en";
            ReadJson(lang);
        };
        sessionStorage.setItem("GlobalLang", lang);
    }else {
        ReadJson(lang);
    }

console.log("variable locale:"+lang)
} else {
    lang = sessionStorage.getItem("GlobalLang");
    ReadJson(lang);
}


